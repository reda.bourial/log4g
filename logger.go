package log4g

import (
	"bytes"
	"fmt"
	"runtime"
	"strings"
	"sync"
	"time"

	"gitlab.com/reda.bourial/catch"
)

type Stringable interface {
	String() string
}

func castToStr(v interface{}) string {
	if str, ok := v.(string); ok {
		return str
	} else if stringable, ok := v.(Stringable); ok {
		return stringable.String()
	}
	return fmt.Sprint(v)
}

func toString(values []interface{}) string {
	var b bytes.Buffer
	for idx, v := range values {
		str := castToStr(v)
		b.WriteString(str)
		if idx != len(values)-1 {
			b.WriteString(" ")
		}
	}
	return b.String()
}

// Logger is an abstract logger.
type Logger interface {
	Log(level string, values ...interface{})
	Append(appendedValues ...interface{}) Logger
	AppendString(appendedMsgs ...string) Logger
	Filter(filteredLevels ...string) Logger
	PrependString(prependedMsgs ...string) Logger
	PrependTime() Logger
	PrependGoRoutines() Logger
	Prepend(prependValues ...interface{}) Logger
	WithLock() Logger
	NoPanic(level string, values ...interface{}) error
	FunCall(args ...interface{}) Logger
	Fatal(values ...interface{})
	Error(values ...interface{})
	Warn(values ...interface{})
	Info(values ...interface{})
	Debug(values ...interface{})
	Trace(values ...interface{})
	All(values ...interface{})
}

type LoggerFun func(level string, values ...interface{})

// MockLogger returns a mock of a logger.
func MockLogger() Logger {
	return LoggerFun(func(level string, values ...interface{}) {
		return
	})
}

// Call  prepends the time of calls to the logger.
func (logger LoggerFun) Log(level string, values ...interface{}) {
	logger(level, values...)
}

// PrependTime prepends the time of calls to the logger.
func (logger LoggerFun) PrependTime() Logger {
	return LoggerFun(func(level string, values ...interface{}) {
		time := time.Now().Format(time.RFC1123)
		logger(level, castToStr(time), toString(values))
	})
}

// PrependGoRoutines prepends the current number of running goroutines.
func (logger LoggerFun) PrependGoRoutines() Logger {
	return LoggerFun(func(level string, values ...interface{}) {
		msg := fmt.Sprint("[ Go routines : ", runtime.NumGoroutine(), " ]")
		logger(level, msg, toString(values))
	})
}

// Prepend the values of loggint to the logger.
func (logger LoggerFun) Prepend(prependValues ...interface{}) Logger {
	prependStr := toString(prependValues)
	return LoggerFun(func(level string, values ...interface{}) {
		logger(level, prependStr, toString(values))
	})
}

// PrependString the strings to the logger.
func (logger LoggerFun) PrependString(prependedMsgs ...string) Logger {
	// convert to interfaces
	prependedValues := make([]interface{}, len(prependedMsgs))
	for i := range prependedMsgs {
		prependedValues[i] = prependedMsgs[i]
	}
	return logger.Prepend(prependedValues...)
}

// FunCall prepend the function call info to the logger.
// The function name is prepended automatically.
// Provide the arguments to log as parameters.
func (logger LoggerFun) FunCall(args ...interface{}) Logger {
	// get Caller name pointer
	fpcs := make([]uintptr, 1)
	runtime.Callers(2, fpcs)
	// get Caller func
	fun := runtime.FuncForPC(fpcs[0])
	// format func name
	funcName := fun.Name()
	// Removing filePath
	if strings.Contains(funcName, ".") {
		if nbPoint := strings.Count(funcName, "."); nbPoint > 0 {
			parts := strings.Split(funcName, ".")[1:]
			funcName = strings.Join(parts, ".")
		}
	}
	header := fmt.Sprintf("-> %s %v :", funcName, args)
	return logger.Prepend(header)
}

// Append values to the logger.
func (logger LoggerFun) Append(appendedValues ...interface{}) Logger {
	return LoggerFun(func(level string, values ...interface{}) {
		logger(level, toString(values), toString(appendedValues))
	})
}

// AppendString append strings to the logger.
func (logger LoggerFun) AppendString(appendedMsgs ...string) Logger {
	appendedValues := make([]interface{}, len(appendedMsgs))
	for i := range appendedMsgs {
		appendedValues[i] = appendedMsgs[i]
	}
	return logger.Append(appendedValues...)
}

// NoPanic intercept an eventual panic and returns it as an error.
func (logger LoggerFun) NoPanic(level string, values ...interface{}) error {
	err := catch.Error(func() {
		logger(level, values...)
	})
	return err
}

// Filter the logging level.
func (logger LoggerFun) Filter(filteredLevels ...string) Logger {
	return LoggerFun(func(level string, values ...interface{}) {
		for _, filteredLevel := range filteredLevels {
			if filteredLevel == level {
				return
			}
		}
		logger(level, values...)
	})
}

// WithLock adds a lock for concurrent writes.
func (logger LoggerFun) WithLock() Logger {
	lock := &sync.Mutex{}
	return LoggerFun(func(level string, values ...interface{}) {
		lock.Lock()
		defer lock.Unlock()
		logger(level, values...)
	})
}

// LoggerFactory dispatch messages and organizes them in topics.
type LoggerFactory func(topic string) Logger

// NoPanic intercept an eventual panic and returns it as an error.
func (lf LoggerFactory) NoPanic(topic string) (Logger, error) {
	var Logger Logger
	err := catch.Error(
		func() {
			Logger = lf(topic)
		})
	return Logger, err
}

// Logger transforms a logger factory to logger.
func (lf LoggerFactory) Logger() Logger {
	return LoggerFun(func(level string, values ...interface{}) {
		lf(level).Log(level, values...)
	})
}

const (
	// FATAL logging level
	FATAL = "[FATAL]"
	// ERROR logging level
	ERROR = "[ERROR]"
	// WARN logging level
	WARN = "[WARN] "
	// INFO logging level
	INFO = "[INFO] "
	// DEBUG logging level
	DEBUG = "[DEBUG]"
	// TRACE logging level
	TRACE = "[TRACE]"
	// ALL logging level
	ALL = "[ALL]  "
)

// Fatal is pretty self explanatory and repetitive
func (logger LoggerFun) Fatal(values ...interface{}) {
	logger(FATAL, values...)
}

// Error repetetive did you say ?
func (logger LoggerFun) Error(values ...interface{}) {
	logger(ERROR, values...)
}

// Warn really, how repetitive ?
func (logger LoggerFun) Warn(values ...interface{}) {
	logger(WARN, values...)
}

// Info not enough to keep me from writing non-sense
func (logger LoggerFun) Info(values ...interface{}) {
	logger(INFO, values...)
}

// Debug just to avoid warnings
func (logger LoggerFun) Debug(values ...interface{}) {
	logger(DEBUG, values...)
}

// Trace now this is getting boring
func (logger LoggerFun) Trace(values ...interface{}) {
	logger(TRACE, values...)
}

// All and that's all !
func (logger LoggerFun) All(values ...interface{}) {
	logger(ALL, values...)
}
