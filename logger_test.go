package log4g

import (
	"fmt"
	"strings"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

type loggerCall struct {
	level  string
	values []interface{}
}

func testFunction2(Logger Logger) {
	Logger = Logger.FunCall("func2Arg2", "func2Arg1")
	Logger.Log(TRACE, "fc1", "fc2")
}
func testFunction(Logger Logger) {
	Logger = Logger.FunCall("func1Arg2", "func1Arg1")
	testFunction2(Logger)
}
func TestLogger(t *testing.T) {
	loggerCalls := make([]loggerCall, 0)
	Logger := LoggerFun(func(level string, values ...interface{}) {
		loggerCalls = append(loggerCalls, loggerCall{
			level:  level,
			values: values,
		})
	}).PrependTime().Prepend("prepend").Append("append").Filter(ALL)
	Logger.Log(ERROR, "msg1", "mgs2")
	Logger.AppendString("a2", "a3").PrependString("p1", "p2").Log(INFO, "msg3", "msg4")
	testFunction(Logger)
	Logger.Log(ALL, "f1", "f2")
	expectedCalls := []loggerCall{
		{level: "[ERROR]",
			values: []interface{}{"Thu, 29 Nov 2018 21:53:07 CET", "prepend msg1 mgs2 append"}},
		{level: "[INFO] ",
			values: []interface{}{"Thu, 29 Nov 2018 21:53:07 CET", "prepend p1 p2 msg3 msg4 a2 a3 append"}},
		{level: "[TRACE]",
			values: []interface{}{"Sun, 03 Oct 2021 18:08:36 CEST", "prepend -> com/reda.bourial/log4g.testFunction [func1Arg2 func1Arg1] : -> com/reda.bourial/log4g.testFunction2 [func2Arg2 func2Arg1] : fc1 fc2 append"}},
	}
	assert.Equal(t, len(expectedCalls), len(loggerCalls))
	for i, expectedCall := range expectedCalls {
		assert.Equal(t, expectedCall.level, loggerCalls[i].level)
		assert.Equal(t, expectedCall.values[1:], loggerCalls[i].values[1:])
	}
	// fmt.Printf("\r\n%#v", loggerCalls)
}

func TestWithLock(t *testing.T) {
	start := time.Now()
	waitime := 100
	logger := LoggerFun(func(string, ...interface{}) {
		time.Sleep(1 * time.Millisecond)
	}).WithLock()
	var wg sync.WaitGroup
	wg.Add(waitime)
	for i := 0; i < waitime; i++ {
		go func() {
			defer wg.Done()
			logger.Log(INFO, i)
		}()
	}
	wg.Wait()
	fmt.Println(time.Now().Sub(start))
	assert.True(t, time.Now().Sub(start) > time.Duration(waitime)*time.Millisecond)
}
func TestLoggerPanicHandle(t *testing.T) {
	Logger := LoggerFun(func(level string, args ...interface{}) {
		panic("hello")
	})
	err := Logger.NoPanic(WARN, "hello")
	assert.NotNil(t, err)
	loggerFactory := LoggerFactory(nil)
	newLogger, err := loggerFactory.NoPanic("hello")
	assert.NotNil(t, err)
	assert.Nil(t, newLogger)
}

func TestMockLogger(t *testing.T) {
	logger := MockLogger()
	logger.Error("something bad")
}

func TestPrependGoRoutines(t *testing.T) {
	logger, logs := NewInMemoryLogger()
	logger.PrependGoRoutines().Debug("showing goroutines")
	output := strings.Join(logs.StringArray(" "), "\r\n")
	assert.True(t, strings.Contains(output, "Go routines : "))
}

type SomeCustomType struct{}

func (s SomeCustomType) String() string {
	return "test"
}

func TestStringable(t *testing.T) {
	logger, logs := NewInMemoryLogger()
	logger.Debug(SomeCustomType{})
	output := strings.Join(logs.StringArray(" "), "\r\n")
	assert.Equal(t, output, "[DEBUG] test ")
}

func TestLevels(t *testing.T) {
	logger, logs := NewInMemoryLogger()
	logger.Fatal(1)
	logger.Error(2)
	logger.Warn(3)
	logger.Info(4)
	logger.Debug(5)
	logger.Trace(6)
	logger.All(7)
	output := strings.Join(logs.StringArray(" "), "|")
	assert.Equal(t, "[FATAL] 1 |[ERROR] 2 |[WARN]  3 |[INFO]  4 |[DEBUG] 5 |[TRACE] 6 |[ALL]   7 ", output)
}

func TestFoo(t *testing.T) {
	logger, logs := NewInMemoryLogger()
	for i := 0; i < 10000; i++ {
		logger = logger.Prepend([]int{0, 1, 2, 3, 4, 5})
	}
	t.Run("test", func(t *testing.T) {
		logger.Info("hello")
	})
	if len(logs.StringArray(" ")) < 0 {
		fmt.Print(logs)
	}
}
func BenchmarkLoop(b *testing.B) {
	logger, logs := NewInMemoryLogger()
	for i := 0; i < 2000; i++ {
		logger = logger.Prepend([]int{0, 1, 2, 3, 4, 5})
	}
	b.Run("test", func(b *testing.B) {
		logger.Info("hello")
	})
	if len(logs.StringArray(" ")) < 0 {
		fmt.Print(logs)
	}
}
