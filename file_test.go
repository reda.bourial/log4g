package log4g

import (
	"bufio"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/reda.bourial/catch"
)

func TestInputStream(t *testing.T) {
	path := "./testdata/testfile"
	badPath := "./f90fc24f-8c9a-4a24-bcdc-8243834e5c76/testfile"
	os.Remove(path)
	t.Run("outputstream", func(t *testing.T) {
		fwc := FileWritingContext{
			CallDelimiter:    "\r\n",
			ValuesDelimiters: " | ",
			Path:             "./testdata/testfile",
		}
		t.Run("initialisation", func(t *testing.T) {
			t.Run("no error", func(t *testing.T) {
				err := fwc.Init()
				assert.Nil(t, err)
			})
			badFwc := FileWritingContext{
				CallDelimiter:    "\r\n",
				ValuesDelimiters: " | ",
				Path:             badPath,
			}
			t.Run("error", func(t *testing.T) {
				err := catch.Error(func() {
					badFwc.Init()
				})
				assert.NotNil(t, err)
			})
			t.Run("bad writer", func(t *testing.T) {
				writer := bufio.Writer{}
				badFwc.writer = &writer
				err := catch.Error(func() {
					badFwc.Init()
				})
				assert.NotNil(t, err)
			})

		})
		t.Run("formatValues", func(t *testing.T) {
			str := fwc.FormatValues("toto", "we right here", 43)
			assert.Equal(t, str, "toto | we right here | 43\r\n")
		})
		fo := fwc.Logger
		// stays opens
		fo.Log("hello", "world", "1")
		fo.Prepend("hello").Log("world", "2")
		err := fo.NoPanic("hello", "world", "3")
		assert.Nil(t, err)
		// closing file
		err = fwc.Close()
		assert.Nil(t, err)
		err = fwc.Close()
		assert.NotNil(t, err)
		err = fo.NoPanic("hello", "world", "4")
		assert.NotNil(t, err)
	})
	t.Run("inputstream", func(t *testing.T) {
		is, err := NewFileInput(path)
		is = is.WithLock()
		assert.Nil(t, err)
		lines := make([]string, 0)
		for line := is(); line != nil; line = is() {
			lines = append(lines, *line)
		}
		assert.Equal(t, []string{"hello | world | 1", "world | hello | 2", "hello | world | 3"}, lines)
		is = nil
		_, err = is.NoPanic()
		assert.NotNil(t, err)
		t.Run("with error", func(t *testing.T) {
			is, err := NewFileInput(badPath)
			assert.Nil(t, is)
			assert.NotNil(t, err)
			panicked, panicErr := catch.Panic(func() {
				var badIs InputStream = nil
				badIs.WithLock()()
			})
			assert.True(t, panicked)
			assert.NotNil(t, panicErr)
		})
	})
}

func TestDirLogger(t *testing.T) {
	folderpath := "./testdata/logs/"
	//badFolderPath := "./f90fc24f-8c9a-4a24-bcdc-8243834e5c76/testfile"
	dirLogger, err := NewDirLogger(FileWritingContext{})
	assert.NotNil(t, err)
	assert.Nil(t, dirLogger)
	os.RemoveAll(folderpath)
	dirLogger, err = NewDirLogger(FileWritingContext{
		FormatingFunc:    func(v interface{}) string { return castToStr(v) },
		CallDelimiter:    "\r\n",
		ValuesDelimiters: " , ",
		Path:             folderpath,
	})
	assert.Nil(t, err)
	loggerFactory := dirLogger.GetLoggerFactory()
	// doesn't panic on multiple access
	loggerFactory("file1")
	fo1 := loggerFactory("file1")
	fo2 := loggerFactory("file2")
	fo1.Log("hello", "world", "1")
	fo2.Log("hello", "world", "2")
	loggerFactory.Logger().Log("file1", "foo", "1")
	loggerFactory.Logger().Log("file2", "foo", "2")
	errs := dirLogger.Close()
	assert.Equal(t, len(errs), 0)
	dirLogger.OpenFiles = []FileWritingContext{
		{
			Path: "unexisting",
		},
	}
	errs = dirLogger.Close()
	assert.Equal(t, len(errs), 1)
	err = fo1.NoPanic("hello", "world", "4")
	assert.NotNil(t, err)
	t.Run("inputstream", func(t *testing.T) {
		t.Run("file1", func(t *testing.T) {
			is, err := NewFileInput("./testdata/logs/file1")
			assert.Nil(t, err)
			lines := make([]string, 0)
			for line := is(); line != nil; line = is() {
				lines = append(lines, *line)
			}
			assert.Equal(t, []string{"hello , world , 1", "file1 , foo , 1"}, lines)
		})
		t.Run("file2", func(t *testing.T) {
			is, err := NewFileInput("./testdata/logs/file2")
			assert.Nil(t, err)
			lines := make([]string, 0)
			for line := is(); line != nil; line = is() {
				lines = append(lines, *line)
			}
			assert.Equal(t, []string{"hello , world , 2", "file2 , foo , 2"}, lines)
		})
	})
}
