package log4g

import "testing"

var bigString = "a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y , z"
var test []interface{} = []interface{}{bigString, bigString, bigString}

var logger Logger = NewConsoleLogger().Append(test...).Prepend(test...).PrependTime().PrependGoRoutines().Prepend(test...).Append(test...)

func BenchmarkInit(b *testing.B) {
	for n := 0; n < b.N; n++ {
		logger = NewConsoleLogger().Append(test...).Prepend(test...).PrependTime().PrependGoRoutines().Prepend(test...).Append(test...)
	}
}

func BenchmarkCalls(b *testing.B) {
	logger.Info("TEST", "Something")
}
