package log4g

import (
	"bytes"
	"container/list"
	"fmt"
)

// InMemoryLogs is an abstraction for logs in memory
type InMemoryLogs struct {
	*list.List
}

// StringArray transforms the log values to string seperated by valueDelimiter.
func (logs InMemoryLogs) StringArray(valueDelimiter string) []string {
	lines := make([]string, logs.Len())
	lineCnt := 0
	for v := logs.Front(); v != nil; v = v.Next() {
		str := fmt.Sprint(v.Value)
		lines[lineCnt] = str
		lineCnt += 1
	}
	return lines
}

// NewInMemoryLogger create a logger that outputs values to buffer.
func NewInMemoryLogger() (Logger Logger, buffer *InMemoryLogs) {
	logBuffer := InMemoryLogs{list.New()}
	return LoggerFun(func(level string, values ...interface{}) {
		var buffer bytes.Buffer
		buffer.WriteString(level)
		buffer.WriteString(" ")
		for _, v := range values {
			str := castToStr(v)
			buffer.WriteString(str)
			buffer.WriteString(" ")
		}
		logBuffer.PushBack(buffer.String())
	}), &logBuffer
}
