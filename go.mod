module gitlab.com/reda.bourial/log4g

go 1.14

require (
	github.com/stretchr/testify v1.6.1
	gitlab.com/reda.bourial/catch v1.0.4
)
